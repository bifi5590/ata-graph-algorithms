package graphalgorithms;

import graphalgorithms.Structures.AdjacencyList;
import graphalgorithms.Structures.AdjacencyMatrix;
import graphalgorithms.Structures.Edge;
import graphalgorithms.Structures.Vertice;

public class GraphAlgorithms {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Test for Adjacency Matrix:
        AdjacencyMatrix matrix = new AdjacencyMatrix(10);
        matrix.setEdge(0, 4, 2.0);
        System.out.println("Distance 0 to 4: "+matrix.getEdge(0, 4) );
        
        //Create Adjacency List:
        AdjacencyList list = new AdjacencyList();
        createSampleGraph(list);
        
        int startVertice = 1;
        
        //BFS using Adjacency List:
        BFS bfs = new BFS(list);
        bfs.start(startVertice);
        
        for(int i=0;i<bfs.getDistances().length; i++) {
            System.out.println("Distance from Node "+startVertice+" to "+i+" is "+bfs.getDistance(i));
        }

        //Prims MST:
        MST_Prim prim = new MST_Prim(list);
        prim.start(startVertice);
        
    }
    
    public static void createSampleGraph(AdjacencyList list) {
        Vertice r = new Vertice("r");
        Vertice s = new Vertice("s");
        Vertice t = new Vertice("t");
        Vertice u = new Vertice("u");
        Vertice v = new Vertice("v");
        Vertice w = new Vertice("w");
        Vertice x = new Vertice("x");
        Vertice y = new Vertice("y");

        r.addEdge(new Edge(s,4));
        r.addEdge(new Edge(v,1));

        v.addEdge(new Edge(r,1));

        s.addEdge(new Edge(w,3));
        s.addEdge(new Edge(r,4));

        w.addEdge(new Edge(s,3));
        w.addEdge(new Edge(t,5));
        w.addEdge(new Edge(x,1));

        t.addEdge(new Edge(w,5));
        t.addEdge(new Edge(x,1));
        t.addEdge(new Edge(u,2));

        u.addEdge(new Edge(t,2));
        u.addEdge(new Edge(x,6));
        u.addEdge(new Edge(y,2));

        x.addEdge(new Edge(w,1));
        x.addEdge(new Edge(t,1));
        x.addEdge(new Edge(u,6));
        x.addEdge(new Edge(y,2));

        y.addEdge(new Edge(x,2));
        y.addEdge(new Edge(u,2));

        list.addVertice(r);
        list.addVertice(s);
        list.addVertice(t);
        list.addVertice(u);
        list.addVertice(v);
        list.addVertice(w);
        list.addVertice(x);
        list.addVertice(y);
        
    }
    
}
