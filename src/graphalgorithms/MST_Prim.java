package graphalgorithms;

import graphalgorithms.Structures.AdjacencyList;
import graphalgorithms.Structures.Edge;
import java.util.PriorityQueue;
import java.util.Queue;

public class MST_Prim {
    
    private AdjacencyList list;
    private Queue<Edge> edges;
    
    //MST: -1 marks start position, other values mark the parent element.
    private int[] T;
    //Distances for the edges from the node to ists parent.
    private int[] D;
    
    public MST_Prim(AdjacencyList list) {
        this.list = list;
        
        //Use a priority Queue. Edge has Comparator to sort for distance
        edges = new PriorityQueue<>();
        
        //Initialise Tree
        T = new int[list.getVerticeCount()];
        D = new int[list.getVerticeCount()];
        
        for(int i=0; i<T.length;i++) {
            T[i] = Integer.MAX_VALUE;
            D[i] = Integer.MAX_VALUE;
        }
        
    }
    
    public void start(int start) {

        //Add edges from start vertice
        edges.addAll(list.getVertice(start).getEdges());
        
        //Get First Element (just to get a start);
        T[start] = -1;
        D[start] = 0;

        while(!edges.isEmpty()) {
            
           Edge e = edges.poll();
           
           //Vertice already visited?
           if(T[e.target.getPosition()] == Integer.MAX_VALUE) {
               //Not visited.
               
               //Set parent:
               T[e.target.getPosition()] = e.source.getPosition();
                
               //Set distance (only the edge distance):
               D[e.target.getPosition()] = e.distance;
               
               //Add Edges of new Vertice to Queue:
               edges.addAll(e.target.getEdges());
           }
           
        }
        
    }
    
    public int[] getMST() {
        return T;
    }
    
    public int[] getDistanceVector() {
        return D;
    }
    
}
