
package graphalgorithms;

import graphalgorithms.Structures.AdjacencyList;
import java.util.LinkedList;
import java.util.Queue;

public class BFS {
    private AdjacencyList list;
    
    //This array stores the distances to all vertices (seen from starting vertice)
    int[] dist;
    
    //BFS using colors to remember state of vertices:
    //0 means unvisited
    //1 means in queue
    //other values mean the vertice has been visited
    int[] color;
    
    //This array stores the predecessor of the vertice (shortest path)
    int[] previous;
    
    
    public BFS(AdjacencyList list) {
        
        //Store the list
        this.list = list;
        
        //Init data arrays:
        dist = new int[list.getVerticeCount()];
        color = new int[list.getVerticeCount()];
        previous = new int[list.getVerticeCount()];
        
        for(int i=0;i<list.getVerticeCount();i++) {
            dist[i] = Integer.MAX_VALUE;
            color[i] = 0;
            previous[i] = 0;
        }
        
    }
    
    //Do a BFS starting from the given vertice
    public void start(int startVertice) {
        
        //Create a new queue for the vertices
        Queue<Integer> queue = new LinkedList<>();
        
        //Set the distance to the start vertice to 0 and add it to the queue
        dist[startVertice] = 0;
        queue.add(startVertice);
        
        //Check all elements of the queue:
        while(!queue.isEmpty()) {
            
            //Get the next item from queue
            int current = queue.poll();
            
            //Check all edges of the selected vertice
            for(int i=0;i<list.getVertice(current).getEdgeCount();i++) {
                
                //Get target vertice from current edge:
                int target = list.getVertice(current).getEdge(i).target.getPosition();
                
                //Color of 0 means the vertice was not visited.
                if(color[target] == 0) {
                    //Add vertice to queue
                    queue.add(target);
                    
                    if (dist[target] > dist[current]+list.getVertice(current).getEdge(i).distance) {
                        //Set the distance to the target: (current distance + edge weight)
                        dist[target] = dist[current]+list.getVertice(current).getEdge(i).distance;
                        //Set predecessor of the target vertice
                        previous[target] = current;
                    }
                    
                    //Update the color to mark it as "in queue"
                    color[target] = color[target]+1;
                }
                
                
            }
            
            //Set color to 3 to mark it as finished
            color[current] = 3;
            
            
        }
        
        System.out.println("Finish");
        
    }
    
    //Return the distance array
    public int[] getDistances() {
        return dist;
    }
    
    //Get the distance to the desired node
    public int getDistance(int n) {
        return dist[n];        
    }
    
}
