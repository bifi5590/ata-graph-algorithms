
package graphalgorithms.Structures;

import java.util.ArrayList;


public class AdjacencyList {
    
    private ArrayList<Vertice> vertices;
    
    public AdjacencyList() {
        vertices = new ArrayList<>();
    }
    
    public void addVertice(Vertice v) {
        v.setPosition(vertices.size());
        vertices.add(v);
    }
    
    public Vertice getVertice(int i) {
        if(i >= 0 && i < vertices.size()) {
            return vertices.get(i);
        }
        return null;
    }
    
    public int getVerticeCount() {
        return vertices.size();
    }
    
}
