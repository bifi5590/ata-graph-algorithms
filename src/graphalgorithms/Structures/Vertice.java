
package graphalgorithms.Structures;

import java.util.ArrayList;

public class Vertice {
    private int position;
    private String name;
    private ArrayList<Edge> edges;
    
    public Vertice(String name) {
        edges = new ArrayList<>();
        this.name = name;
    }
    
    public void addEdge(Edge e) {
        e.source = this;
        edges.add(e);
    }
    
    public Edge getEdge(int i) {
        if(i >= 0 && i <= edges.size()) {
            return edges.get(i);
        }else{
            return null;
        }
    }
    
    public ArrayList<Edge> getEdges() {
        return edges;
    }
    
    public String getName() {
        return name;
    }
    
    public void setPosition(int n) {
        this.position = n;
    }
    
    public int getPosition() {
        return this.position;
    }
    
    public int getEdgeCount() {
        return edges.size();
    }
    
}
