
package graphalgorithms.Structures;

public class AdjacencyMatrix {
    
    double[][] matrix;
    int nodes = 0;
    
    public AdjacencyMatrix(int n) {
        matrix = new double[n][n];
        nodes = n;
    }
    
    public boolean setEdge(int x, int y, double distance) {
        if(x < nodes && y < nodes && x >= 0 && y >= 0) {
            matrix[x][y] = distance;
            return true;
        }
        return false;
    }
    
    public double getEdge(int x, int y) {
        if(x < nodes && y < nodes && x >= 0 && y >= 0) {
            return matrix[x][y];
        }
        return 0.0;
    }
    
    public boolean removeEdge(int x, int y) {
        if(x < nodes && y < nodes && x >= 0 && y >= 0) {
            matrix[x][y] = 0;
            return true;
        }
        return false;
    }
    
    
}
