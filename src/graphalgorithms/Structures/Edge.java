
package graphalgorithms.Structures;

public class Edge implements Comparable<Edge>{
    public Vertice target;
    public Vertice source;
    public int distance;
    
    public Edge(Vertice target, int distance) {
        this.target = target;
        this.distance = distance;
    }

    @Override
    public int compareTo(Edge o) {
        if(distance < o.distance) {
            return -1;
        }
        if(distance > o.distance) {
            return 1;
        }
        return 0;
    }
    
}
